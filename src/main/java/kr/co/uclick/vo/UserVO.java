package kr.co.uclick.vo;

public class UserVO {

	private int Eblock;
	private int Sblock;
	private int totalpage;
	private int before;
	private int next;

	public int getEblock() {
		return Eblock;
	}

	public void setEblock(int eblock) {
		Eblock = eblock;
	}

	public int getSblock() {
		return Sblock;
	}

	public void setSblock(int sblock) {
		Sblock = sblock;
	}

	public int getTotalpage() {
		return totalpage;
	}

	public void setTotalpage(int totalpage) {
		this.totalpage = totalpage;
	}

	public int getBefore() {
		return before;
	}

	public void setBefore(int before) {
		this.before = before;
	}

	public int getNext() {
		return next;
	}

	public void setNext(int next) {
		this.next = next;
	}

}
