package kr.co.uclick.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.jdo.annotations.Transactional;
import javax.servlet.http.HttpSession;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import kr.co.uclick.entity.Phone;
import kr.co.uclick.entity.User;
import kr.co.uclick.service.PhoneService;
import kr.co.uclick.service.UserService;

@Controller
public class UserController {

	@Autowired
	private UserService userService;

	@Autowired
	private PhoneService phoneService;

	/*
	 * ***** 로그인 페이지 (메인페이지) *****
	 */
	@RequestMapping(value = "login")
	public String login(Model model) {
		return "login";
	}

	/*
	 * ***** 로그인 확인 ***** 로그인 성공 / 로그인 실패 / 세션 부여
	 */
	@RequestMapping(value = "loginOK")
	public String loginOK(Model model, String usesrid, String passwd, HttpSession session) {

		/*
		 * ***** 해당 아이디와 비밀번호를 DB와 체크 ***** 일치할 경우 true
		 */
		int result = userService.IdCk(usesrid);
		if( result > 0)
		{
			String hashedPw = userService.findByUserId(usesrid).getPassword();
			if (hashedPw != null) {
				boolean isValidPassword = BCrypt.checkpw(passwd, hashedPw);
				if (isValidPassword == true) {
					/*
					 * ***** 세션을 부여하기 위한 절차 *****
					 */
					String name = userService.findByUserId(usesrid).getName();
					Long id = userService.findByUserId(usesrid).getId();
					/*
					 * ***** 세션 부여 *****
					 */
					session.setAttribute("sessionuserid", usesrid); // 사용자 계정 아이디
					session.setAttribute("sessionname", name);
					session.setAttribute("sessionid", id); // 사용자 계정 번호 ID
					model.addAttribute("id", id);
					model.addAttribute("msg", "true");
				}else {
					model.addAttribute("msg", "false");
				}
			} 
		}else {
			model.addAttribute("msg", "error");
		}

		return "loginOK";
	}

	/*
	 * ***** 로그아웃 페이지 **** 세션 일괄 삭제
	 */
	@RequestMapping(value = "logout")
	public String logout(Model model, HttpSession session) {
		/*
		 * ***** 세션이 없을 경우 로그인페이지 자동 이동 ***** 모든 페이지에 공통 적용됨. (login, loginOK 제외)
		 */
		Long sessionid = (Long) session.getAttribute("sessionid");
		if (sessionid == null) {
			return "redirect:login";
		}
		/*
		 * 세션 일괄 삭제
		 */
		session.invalidate();

		return "logout";
	}

	/*
	 * ***** 회원가입 페이지 *****
	 */
	@RequestMapping(value = "signup")
	public String signup(Model model) {
		return "signup";
	}

	/*
	 * ***** 회원가입 확인 페이지 *****
	 */
	@RequestMapping(value = "signOK")
	public String signOK(Model model, String mobileNo1, String mobileNo2, String mobileNo3,
			@RequestParam HashMap<String, String> map, HttpSession session) {
		String id = map.get("id");
		String passwd = map.get("passwd");
		String username = map.get("username");
		String mobileNo = mobileNo1 + "-" + mobileNo2 + "-" + mobileNo3;
		String loc = map.get("loc");
		String dept = map.get("dept");
		String position = map.get("position");
		String email = map.get("email");

		/*
		 * *****아이디 중복체크 및 전화번호 중복체크***** 아이디가 중복될 경우 (result가 양수) msg가 false 전화번호가 중복될
		 * 경우 (equal가 양수) msg가 equal 그 외의 경우 msg가 true => 정상적인 등록
		 */
		int result = userService.IdCk(id);
		if (result > 0) {
			model.addAttribute("msg", "false");
		} else {
			int equal = phoneService.findByPhoneNoExist(mobileNo);
			if (equal > 0) {
				model.addAttribute("msg", "equal");
			} else {
				String hashpw = BCrypt.hashpw(passwd, BCrypt.gensalt(10));
				userService.newUser(id, hashpw, username, mobileNo, loc, dept, position, email);
				model.addAttribute("msg", "true");
			}
		}

		return "signOK";
	}

	/*
	 * ***** 전체보기 페이지 ***** 페이지네이션기능 / 검색기능
	 */
	@Transactional
	@RequestMapping(value = "findAll")
	public String findAll(Model model, @RequestParam HashMap<String, String> map, HttpSession session) {
		String opt = map.get("opt");
		String search = map.get("search");
		int page = 0;
		String pages = map.get("page");
		if (pages == null) {
			page = 1;
		} else {
			page = Integer.parseInt(pages);
		}

		/*
		 * ***** 검색 opt와 검색어 searching에 따른 화면 출력 ***** 검색어가 없으면 findAll 출력 검색어가 있으면 세
		 * 경우의 수 (근무지 / 부서 / 전화번호) 검색어에 따라 해당 메소드의 결과로 출력
		 */
		boolean searching = false;
		List<User> user_list = null;

		if (search == null || search.equals("")) {
			searching = false;
			user_list = userService.findAll(page - 1);
		} else {
			user_list = new ArrayList<User>();
			searching = true;
			if (opt.equals("name")) {
				user_list = userService.findAllByName(search, page - 1);
			} else if (opt.equals("loc")) {
				user_list = userService.findAllByLoc(search, page - 1);
			} else if (opt.equals("dept")) {
				user_list = userService.findAllByDept(search, page - 1);
			} else if (opt.equals("tel")) {
				List<Phone> phone_list = phoneService.findPhonesBymobileNO(search);
				if (phone_list != null) {
					for (int i = 0; i < phone_list.size(); i++) {
						user_list.add(phone_list.get(i).getUser());
					}
				}
			}
		}

		int Sblock = userService.userVO(page, searching, opt, search).getSblock();
		int Eblock = userService.userVO(page, searching, opt, search).getEblock();
		int totalpage = userService.userVO(page, searching, opt, search).getTotalpage();
		int Before = userService.userVO(page, searching, opt, search).getBefore();
		int Next = userService.userVO(page, searching, opt, search).getNext();
		if (Eblock > totalpage) {
			Eblock = totalpage;
		}

		/*
		 * ***** 세션 여부에 따른 findAll 화면 출력 ***** 세션이 없으면 login화면으로 이동 세션이 있으면 세션으로 해당 사용자
		 * id로 값 생성
		 */

		Long sessionid = (Long) session.getAttribute("sessionid");
		Long id;
		if (sessionid == null) {
			return "redirect:login";
		} else {
			id = sessionid;
		}

		User user = userService.findById(sessionid);
		int size = phoneService.findPhoneByUser_id(sessionid).size();
		String mainphone = null;
		if (size > 0) {
			mainphone = phoneService.findPhoneByUser_id(sessionid).get(0).getMobileNo();
		}

		model.addAttribute("id", id);
		model.addAttribute("user", user);
		model.addAttribute("mainphone", mainphone);
		model.addAttribute("searching", searching);
		model.addAttribute("user_list", user_list);
		model.addAttribute("pages", totalpage);
		model.addAttribute("page", page);
		model.addAttribute("opt", opt);
		model.addAttribute("search", search);
		model.addAttribute("Sblock", Sblock);
		model.addAttribute("Eblock", Eblock);
		model.addAttribute("Before", Before);
		model.addAttribute("Next", Next);

		return "findAll";
	}

	/*
	 * ***** 개별보기 페이지 ***** 특정 사용자의 근무지/부서/직급/휴대전화 리스트 등 조회
	 */
	@RequestMapping(value = "view")
	public String view(Model model, Long user_id, Integer page, HttpSession session) {
		Long sessionid = (Long) session.getAttribute("sessionid");
		Long id;
		if (sessionid == null) {
			return "redirect:login";
		}
		if (user_id == null) {
			return "redirect:findAll";
		}
		/*
		 * ***** direct로 view페이로 들어왔을 경우 ***** page가 파라미터로 전송되지 않는 에러 대비 초기값 설정
		 */
		if (page == null) {
			page = 1;
		}
		/*
		 * findAll 페이지에서의 user_id와 id 혼동으로 인한 user_id(조회하고자 하는 계정의 id) vs id(접속한 계정의 id
		 * & 돌아갈 페이지의 id이기도 함)
		 */

		User user = userService.findById(user_id);
		List<Phone> user_phones = phoneService.findPhoneByUser_id(user_id);
		int size = phoneService.findPhoneByUser_id(sessionid).size();
		String mainphone = null;
		if (size > 0) {
			mainphone = phoneService.findPhoneByUser_id(sessionid).get(0).getMobileNo();
		}
		model.addAttribute("user", user);
		model.addAttribute("mainphone", mainphone);
		model.addAttribute("user_phones", user_phones);
		model.addAttribute("page", page);
		return "view";
	}

	/*
	 * ***** 사용자 계정 (전화번호 조회)페이지 *****
	 */
	@RequestMapping(value = "personal")
	public String personal(Model model, HttpSession session) {
		Long sessionid = (Long) session.getAttribute("sessionid");
		Long id;
		if (sessionid == null) {
			return "redirect:login";
		} else {
			id = sessionid;
		}

		User user = userService.findById(id);
		List<Phone> user_phones = phoneService.findPhoneByUser_id(id);
		int size = phoneService.findPhoneByUser_id(sessionid).size();
		String mainphone = null;
		if (size > 0) {
			mainphone = phoneService.findPhoneByUser_id(sessionid).get(0).getMobileNo();
		}
		model.addAttribute("user", user);
		model.addAttribute("id", id);
		model.addAttribute("mainphone", mainphone);
		model.addAttribute("user_phones", user_phones);

		return "personal";
	}

	/*
	 * ***** 사용자 계정 (전화번호 편집)페이지 *****
	 */
	@RequestMapping(value = "edit")
	public String edit(Model model, String msg, HttpSession session) {
		Long sessionid = (Long) session.getAttribute("sessionid");
		Long id;
		if (sessionid == null) {
			return "redirect:login";
		} else {
			id = sessionid;
		}

		User user = userService.findById(id);
		List<Phone> user_phones = phoneService.findPhoneByUser_id(id);
		int size = phoneService.findPhoneByUser_id(sessionid).size();
		String mainphone = null;
		if (size > 0) {
			mainphone = phoneService.findPhoneByUser_id(sessionid).get(0).getMobileNo();
		}
		model.addAttribute("user", user);
		model.addAttribute("id", id);
		model.addAttribute("msg", msg);
		model.addAttribute("mainphone", mainphone);
		model.addAttribute("user_phones", user_phones);

		return "edit";
	}

	/*
	 * ***** 사용자 계정 (전화번호 수정)페이지 *****
	 */
	@RequestMapping(value = "update")
	public String update(Model model, Long id, String MobileNO, HttpSession session) {
		Long sessionid = (Long) session.getAttribute("sessionid");
		if (sessionid == null) {
			return "redirect:login";
		}

		int exist = phoneService.findByPhoneNoExist(MobileNO);
		if (exist > 0) {
			model.addAttribute("msg", "false");
		} else {
			phoneService.update(id, MobileNO);
		}

		return "redirect:edit?";
	}

	/*
	 * ***** 사용자 계정 (전화번호 삭제)페이지 *****
	 */
	@RequestMapping(value = "delete")
	public String delete(Model model, Long id, HttpSession session) {
		Long sessionid = (Long) session.getAttribute("sessionid");
		if (sessionid == null) {
			return "redirect:login";
		}

		phoneService.delete(id);
		return "redirect:edit";
	}

	/*
	 * ***** 사용자 계정 (전화번호 추가)페이지 *****
	 */
	@RequestMapping(value = "add")
	public String add(Model model, String mobileNo, HttpSession session) {
		Long sessionid = (Long) session.getAttribute("sessionid");
		Long id;
		if (sessionid == null) {
			return "redirect:login";
		} else {
			id = sessionid;
		}
		int exist = phoneService.findByPhoneNoExist(mobileNo);
		if (exist > 0) {
			model.addAttribute("msg", "false");
		} else {
			phoneService.create(id, mobileNo);
		}
		return "redirect:edit";
	}

	/*
	 * ***** 계정관리 페이지 *****
	 */
	@RequestMapping(value = "account")
	public String account(Model model, HttpSession session) {
		Long sessionid = (Long) session.getAttribute("sessionid");
		Long id;
		if (sessionid == null) {
			return "redirect:login";
		} else {
			id = sessionid;
		}

		User user_account = userService.findById(id);
		model.addAttribute("id", id);
		model.addAttribute("user_account", user_account);
		return "account";
	}

	/*
	 * ***** 계정 삭제 페이지 *****
	 */
	@RequestMapping(value = "accountdelete")
	public String accountdelete(Model model, HttpSession session) {
		Long sessionid = (Long) session.getAttribute("sessionid");
		Long id;
		if (sessionid == null) {
			return "redirect:login";
		} else {
			id = sessionid;
		}

		userService.delete(id);
		model.addAttribute("id", id);
		model.addAttribute("msg", "true");

		return "accountdelete";
	}

	/*
	 * ***** 계정수정 페이지 *****
	 */
	@RequestMapping(value = "accountchange")
	public String accountchange(Model model, String email, HttpSession session) {
		Long sessionid = (Long) session.getAttribute("sessionid");
		Long id;
		if (sessionid == null) {
			return "redirect:login";
		} else {
			id = sessionid;
		}

		User user_account = userService.findById(id);
		model.addAttribute("id", id);
		model.addAttribute("user_account", user_account);

		return "accountchange";
	}

	/*
	 * ***** 계정수정확인 페이지 *****
	 */
	@RequestMapping(value = "accountchangeend")
	public String accountchangeend(Model model, String email, HttpSession session) {
		Long sessionid = (Long) session.getAttribute("sessionid");
		Long id;
		if (sessionid == null) {
			return "redirect:login";
		} else {
			id = sessionid;
		}

		userService.update(id, email);

		return "redirect:account?id=" + id;
	}
}
