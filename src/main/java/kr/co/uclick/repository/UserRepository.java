package kr.co.uclick.repository;

import javax.persistence.QueryHint;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import kr.co.uclick.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	// 이름검색
	User findByName(String name);

	User findByUserid(String userid);

	// 이름 검색
	@QueryHints(value = { 
			@QueryHint(name = "org.hibernate.cacheable", value = "true"),
			@QueryHint(name = "org.hibernate.cacheMode", value = "NORMAL") })
	Page<User> findAllByNameLike(String name, Pageable pageable);

	// 근무지 검색
	@QueryHints(value = { 
			@QueryHint(name = "org.hibernate.cacheable", value = "true"),
			@QueryHint(name = "org.hibernate.cacheMode", value = "NORMAL") })
	Page<User> findAllByLocContaining(String loc, Pageable pageable);

	// 부서 검색
	@QueryHints(value = { 
			@QueryHint(name = "org.hibernate.cacheable", value = "true"),
			@QueryHint(name = "org.hibernate.cacheMode", value = "NORMAL") })
	Page<User> findAllByDeptContaining(String dept, Pageable pageable);

	// userid가 존재하는지 유무 판단하기 위한 메소드(아이디 중복체크용)
	@QueryHints(value = { 
			@QueryHint(name = "org.hibernate.cacheable", value = "true"),
			@QueryHint(name = "org.hibernate.cacheMode", value = "NORMAL") })
	@Query("select count(u) from User u where u.userid=:userid")
	int count(@Param("userid") String userid);

}
