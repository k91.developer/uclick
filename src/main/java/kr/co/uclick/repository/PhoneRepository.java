package kr.co.uclick.repository;

import java.util.List;

import javax.persistence.QueryHint;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import kr.co.uclick.entity.Phone;

@Repository
public interface PhoneRepository extends JpaRepository<Phone, Long> {

	Phone findAllBymobileNo(String mobileNo);
	
	// 전화번호 검색
	@QueryHints(value = { 
			@QueryHint(name = "org.hibernate.cacheable", value = "true"),
			@QueryHint(name = "org.hibernate.cacheMode", value = "NORMAL") })
	public List<Phone> findAllBymobileNoContaining(String mobileNo);

	@QueryHints(value = { 
			@QueryHint(name = "org.hibernate.cacheable", value = "true"),
			@QueryHint(name = "org.hibernate.cacheMode", value = "NORMAL") })
	List<Phone> findPhoneByUser_id(Long user_id);

	// phoneNO가 존재하는지 유무 판단하기 위한 메소드(연락처 중복체크용)
	@QueryHints(value = { 
			@QueryHint(name = "org.hibernate.cacheable", value = "true"),
			@QueryHint(name = "org.hibernate.cacheMode", value = "NORMAL") })
	@Query("select count(u) from Phone u where u.mobileNo=:mobileNo")
	int count(@Param("mobileNo") String mobileNo);

}
