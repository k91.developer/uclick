package kr.co.uclick.service;

import java.util.List;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.co.uclick.entity.Phone;
import kr.co.uclick.entity.User;
import kr.co.uclick.repository.PhoneRepository;
import kr.co.uclick.repository.UserRepository;
import kr.co.uclick.vo.UserVO;

@Service
@Transactional
public class UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PhoneRepository phoneRepository;

	// CREATE
	@CacheEvict(value = "area", allEntries = true)
	public void newUser(String id, String passwd, String username, String mobileNo, String loc, String dept,
			String position, String email) {
		User u = new User();
		u.setUserid(id);
		u.setPassword(passwd);
		u.setName(username);
		u.setLoc(loc);
		u.setDept(dept);
		u.setPosition(position);
		u.setEmail(email);
		Phone p = new Phone();
		p.setMobileNo(mobileNo);
		p.setUser(u);
		userRepository.save(u);
		phoneRepository.save(p);

	}

	// READ
	@Cacheable(value = "area")
	@Transactional(readOnly = true)
	public List<User> findAll(int page) {
		List<User> userList = userRepository.findAll(PageRequest.of(page, 10)).getContent();

		for (User u : userList) {
			Hibernate.initialize(u.getPhones());
		}

		return userList;
	}

	// READ
	@Cacheable(value = "area")
	@Transactional(readOnly = true)
	public List<User> findAllByName(String name, int page) {
		List<User> userList = userRepository.findAllByNameLike("%" + name + "%", PageRequest.of(page, 10)).getContent();

		for (User u : userList) {
			Hibernate.initialize(u.getPhones());
		}

		return userList;
	}

	// READ
	@Cacheable(value = "area")
	@Transactional(readOnly = true)
	public List<User> findAllByLoc(String loc, int page) {
		List<User> userList = userRepository.findAllByLocContaining(loc, PageRequest.of(page, 10)).getContent();

		for (User u : userList) {
			Hibernate.initialize(u.getPhones());
		}

		return userList;
	}
 
	// READ
	@Cacheable(value = "area")
	@Transactional(readOnly = true)
	public List<User> findAllByDept(String dept, int page) {
		List<User> userList = userRepository.findAllByDeptContaining(dept, PageRequest.of(page, 10)).getContent();

		for (User u : userList) {
			Hibernate.initialize(u.getPhones());
		}

		return userList;
	}

	// UPDATE
	@CacheEvict(value = "area", allEntries=true)
	public void update(Long id, String email) {
		User u = userRepository.findById((long) id).get();
		u.setEmail(email);
		userRepository.save(u);
	}

	// DELETE
	@CacheEvict(value = "area", allEntries=true)
	public void delete(long id) {
		userRepository.deleteById(id);
	}

	@Cacheable(value = "area")
	@Transactional(readOnly = true)
	public User findByName(String name) {
		return userRepository.findByName(name);

	}

	@Cacheable(value = "area")
	@Transactional(readOnly = true)
	public User findById(Long id) {
		return userRepository.findById(id).get();
	}

	// PAGENATION
	public UserVO userVO(int page, boolean searching, String opt, String value) {
		int totalpage = 0;
		if (searching == false) { // 미검색
			totalpage = userRepository.findAll(PageRequest.of(0, 10)).getTotalPages();
		} else if (opt.equals("loc")) { // 근무지 검색
			totalpage = userRepository.findAllByLocContaining(value, PageRequest.of(0, 10)).getTotalPages();
		} else if (opt.equals("dept")) { // 부서 검색
			totalpage = userRepository.findAllByDeptContaining(value, PageRequest.of(0, 10)).getTotalPages();
		} else if (opt.equals("name")) { // 이름 검색
			totalpage = userRepository.findAllByNameLike("%" + value + "%", PageRequest.of(0, 10)).getTotalPages();
		} else {

		}
		// 블럭 수
		int block = 10;
		// 블럭 시작
		int Sblock = (page - 1) / block * block + 1;
		// 블럭끝
		int Eblock = Sblock + 9;
		if (Eblock > totalpage)
			Eblock = totalpage;
		// 이전
		int Before = Sblock - 10;
		if (Before < 0)
			Before = 1;
		// 다음
		int Next = Sblock + 10;
		if (Next > totalpage)
			Next = totalpage;

		UserVO p = new UserVO();
		p.setBefore(Before);
		p.setSblock(Sblock);
		p.setEblock(Eblock);
		p.setNext(Next);
		p.setTotalpage(totalpage);

		return p;
	}

	public User findByUserId(String userid) {
		return userRepository.findByUserid(userid);
	}

	public int IdCk(String id) {
		return userRepository.count(id);
	}
}
