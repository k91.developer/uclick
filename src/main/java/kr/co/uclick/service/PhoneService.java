package kr.co.uclick.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.co.uclick.entity.Phone;
import kr.co.uclick.entity.User;
import kr.co.uclick.repository.PhoneRepository;
import kr.co.uclick.repository.UserRepository;

@Service
@Transactional
public class PhoneService {

	@Autowired
	private PhoneRepository phoneRepository;

	@Autowired
	private UserRepository userRepository; 

	// CREATE
	@CacheEvict(value = "area", allEntries=true)
	public void create(Long id, String mobileNO) {
		User u = userRepository.findById(id).get();
		Phone p = new Phone();
		p.setMobileNo(mobileNO);
		p.setUser(u);
		phoneRepository.save(p);
	}

	// READ
	@Cacheable(value = "area")
	@Transactional(readOnly = true)
	public List<Phone> findAll() {
		return phoneRepository.findAll();
	}

	// READ
	@Cacheable(value = "area")
	@Transactional(readOnly = true)
	public List<Phone> findPhonesBymobileNO(String mobileNo) {
		return phoneRepository.findAllBymobileNoContaining(mobileNo);
	}

	// READ
	@Cacheable(value = "area")
	@Transactional(readOnly = true)
	public List<Phone> findPhoneByUser_id(Long user_id) {
		return phoneRepository.findPhoneByUser_id(user_id);
	}

	// UPDATE
	@CacheEvict(value = "area", allEntries=true)	
	public void update(Long id, String mobileNO) {
		Phone p = phoneRepository.findById(id).get();
		p.setMobileNo(mobileNO);
		phoneRepository.save(p);
	}

	// DELETE
	@CacheEvict(value = "area", allEntries=true)
	public void delete(Long id) {
		phoneRepository.deleteById(id);
	}

	@Cacheable(value = "area")
	@Transactional(readOnly = true)
	public Phone findPhoneBymobileNO(String mobileNo) {
		return phoneRepository.findAllBymobileNo(mobileNo);
	}

	@Cacheable(value = "area")
	@Transactional(readOnly = true)
	public int findByPhoneNoExist(String mobileNO) {
		return phoneRepository.count(mobileNO);
	}

}
