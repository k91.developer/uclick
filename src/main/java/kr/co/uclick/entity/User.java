package kr.co.uclick.entity;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.CacheConcurrencyStrategy;

@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Entity
public class User {

	@Id
	@GeneratedValue
	@Column
	private Long id;

	@Column(nullable = false)
	@Size(min = 4, max = 10, message = "아이디를 4~10자로 입력해주세요.")
	private String userid;

	@Column(nullable = false)
	@Size(min = 4, max = 12, message = "비밀번호를 4~12자로 입력해주세요.")
	private String password;

	@Column(nullable = false, length = 20)
	private String name;

	@Column(nullable = false)
	private String loc;

	@Column(nullable = false)
	private String dept;

	@Column(nullable = false)
	private String position;

	@Column
	@Email
	private String email;

	@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONE)
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
	private Collection<Phone> phones;

	public User() {
	}

	public User(String name) {
		this.name = name;
	}

//	@Override
//	public String toString() {
//		String result = "[USERID_" + id + "]" + name;
//		for(Phone p : getPhones()) {
//			result += "\n" + p.toString();
//		}
//		return result;
//	}

	public Long getId() {
		return id;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLoc() {
		return loc;
	}

	public void setLoc(String loc) {
		this.loc = loc;
	}

	public String getDept() {
		return dept;
	}

	public void setDept(String dept) {
		this.dept = dept;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Collection<Phone> getPhones() {
		if (phones == null) {
			phones = new ArrayList<Phone>();
		}
		return phones;
	}

	public void setPhones(Collection<Phone> phones) {
		this.phones = phones;
	}

	public void addPhone(Phone p) {
		Collection<Phone> phones = getPhones();
		p.setUser(this);
		phones.add(p);
	}

	// 아이디 중복확인 확인 위한 메서드
	public boolean isIdEqualToCheckId(Long id, String checkId) {
		return id.equals(checkId);
	}

}
