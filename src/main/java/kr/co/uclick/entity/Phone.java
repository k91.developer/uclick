package kr.co.uclick.entity;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.CacheConcurrencyStrategy;

@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Entity
public class Phone {

	@Id
	@GeneratedValue
	@Column
	private Long id;

	@Column(nullable = false)
	private String mobileNo;

	@ManyToOne(optional = false)
	@JoinColumn(name = "user_id")
	private User user;

	public Phone() {
	}

	public Phone(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	@Override
	public String toString() {
		String result = "[PHONE_" + id + "]" + mobileNo;
		return result;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
