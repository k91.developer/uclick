// 전화번호 추가 edit.jsp
function add(){
	var rgExpPhone = /^01([0|1|6|7|8|9]?)-([0-9]{3,4})-([0-9]{4})$/;
	var objs = document.fr;

	if(!rgExpPhone.test(objs.mobileNo.value)){
		alert("잘못된 휴대폰 번호입니다. 숫자만 입력하세요."); 
		objs.mobileNo.focus();
		return false;
	}

	else if(confirm("등록 하시겠습니까?")){
		objs.submit();
    }
}

// 전화번호 수정 edit.jsp
function update(idx){
	var rgExpPhone = /^01([0|1|6|7|8|9]?)-([0-9]{3,4})-([0-9]{4})$/;
	var objs = document.forms[idx];
	var Mobileno = prompt("연락처를 입력하세요");
	console.log(Mobileno);
	if(Mobileno!=null){
		var dom = document.getElementById("ph" + idx)
		dom.value = Mobileno;
// 		console.log(dom);
// 		document.getElementById("ph").value=Mobileno;
// 		console.log(Mobileno);	
		if(!rgExpPhone.test(dom.value)){
			alert("잘못된 휴대폰 번호입니다. - 를 포함한 숫자만 입력하세요.");
			return false;
		}
		else if(confirm("등록 하시겠습니까?")){
			objs.submit();
	    }	
	}
// 	console.log(document.up);
// 	console.log(obj);
// 	console.log(idx);
}

function accountchange(){
	var obj = document.fr;
	var regExpEmail = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;
	if(obj.email.value != '') {
		if(!regExpEmail.test(obj.email.value)){
			alert("잘못된 이메일입니다.");
			obj.email.focus();  
			return false; 
		}
		else if(confirm("등록 하시겠습니까?")){
			obj.submit();
	    }
	}
	else if(confirm("등록 하시겠습니까?")){
		obj.submit();
    }
}

// 전화번호 자동 하이픈 입력 edit.jsp
function inputPhoneNumber(obj) {

    var number = obj.value.replace(/[^0-9]/g, "");
    var phone = "";

    if(number.length < 4) {
        return number;
    } else if(number.length < 7) {
        phone += number.substr(0, 3);
        phone += "-";
        phone += number.substr(3);
    } else if(number.length < 11) {
        phone += number.substr(0, 3);
        phone += "-";
        phone += number.substr(3, 3);
        phone += "-";
        phone += number.substr(6);
    } else {
        phone += number.substr(0, 3);
        phone += "-";
        phone += number.substr(3, 4);
        phone += "-";
        phone += number.substr(7);
    }
    obj.value = phone;
}

// 회원가입 유효성 signup.jsp
function signup(){
	var obj = document.fr;
	var regExpId = /^\S*$/;
	var regExpPasswd = /^\S*$/;
	var regExpPhone = /^01([0|1|6|7|8|9]?)-([0-9]{3,4})-([0-9]{4})$/;
	var strvalue = obj.mobileNo1.value+"-"+obj.mobileNo2.value+"-"+obj.mobileNo3.value;
 	var regExpEmail = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;
 	var regExpName = /^[가-힣a-zA-Z]*$/;
	if(obj.id.value == ''){
		alert("아이디를 입력하세요"); 
		obj.id.focus();
		return false;
	}
	else if(!regExpId.test(obj.id.value)){
		alert("공백을 제외하여 아이디를 입력해주세요");
		obj.id.value="";
		obj.id.focus();
		return false;
	}
	else if(obj.id.value.length < 4 || obj.id.value.length > 10){
		alert("아이디는 4~10자 사이로 입력하세요"); 
		obj.id.focus();
		return false;
	}
	else if(obj.passwd.value == ''){
		alert("비밀번호를 입력해 주세요"); 
		obj.passwd.focus();
		return false;
	}
	else if(!regExpPasswd.test(obj.passwd.value)){
		alert("공백을 제외하여 비밀번호를 입력해주세요");
		obj.passwd.value="";
		obj.passwd.focus();
		return false;
	}
	else if(obj.passwd.value.length < 4 || obj.passwd.value.length > 12){
		alert("비밀번호를 4~12자로 입력해주세요."); 
		obj.passwd.focus();
		return false;
	}
	else if(obj.passwdCk.value == ''){
		alert("비밀번호를 입력해 주세요"); 
		obj.passwdCk.focus();
		return false;
	}
	else if(obj.passwd.value != obj.passwdCk.value){
		alert("비밀번호가 서로 다릅니다. 비밀번호를 확인해 주세요."); 
		obj.passwdCk.focus();
		return false;
	}
	else if(obj.username.value == '' || obj.username.value.length > 20){
		alert("이름을 입력해 주세요 (20byte이하)"); 
		obj.username.value="";
		obj.username.focus();
		return false;
	}
	else if(!regExpName.test(obj.username.value)){
		alert("한글과 영어만 입력해 주세요.");
		obj.username.value="";
		obj.username.focus();
		return false;
	}
	else if(obj.mobileNo1.value == ''){
		alert("연락처를 입력해 주세요.");
		obj.mobileNo1.focus();
		return false;
	}
	else if(obj.mobileNo2.value == ''){
		alert("연락처를 입력해 주세요.");
		obj.mobileNo1.focus();
		return false;
	}
	else if(obj.mobileNo3.value == ''){
		alert("연락처를 입력해 주세요.");
		obj.mobileNo3.focus();
		return false;
	}
	else if(!regExpPhone.test(strvalue)){
		alert("잘못된 휴대폰 번호입니다. 숫자, - 를 포함한 숫자만 입력하세요.");
		obj.mobileNo1.focus();  
		return false; 
	}
	else if(obj.loc.value == ''){
		alert("근무지를 입력해 주세요"); 
		obj.loc.focus();
		return false;
	}
	else if(obj.dept.value == ''){
		alert("부서를 입력해 주세요"); 
		obj.dept.focus();
		return false;
	}
	else if(obj.position.value == ''){
		alert("직위를 입력해 주세요"); 
		obj.position.focus();
		return false;
	}
	else if(obj.email.value != '') {
		if(!regExpEmail.test(obj.email.value)){
			alert("잘못된 이메일입니다.");
			obj.email.focus();  
			return false; 
		}else if(confirm("등록 하시겠습니까?")){
			obj.submit();
	    }
	}
	else if(confirm("등록 하시겠습니까?")){
		obj.submit();
    }
    obj.reset();
    document.getElementById("alert_text").innerHTML=('<span style="color:#777">아이디를 입력해주세요</span>')
    document.getElementById("alert_pw").innerHTML=('<span style="color:#777">비밀번호를 입력해주세요.</span>')
    document.getElementById("alert_pwd").innerHTML=('<span style="color:#777">비밀번호를 한번 더 입력해주세요.</span>')
}

//아이디 유효성 체크 signup.jsp
function idck(){
	var obj = document.fr;
	if(obj.id.value.length<4 || obj.id.value.lenth>10){
		document.getElementById("alert_text").innerHTML=('<span style="color:red;">아이디는 4~10자 사이로 입력해주세요</span>')
		return;		
	}else{
		document.getElementById("alert_text").innerHTML=('<span style="color:green;">정상적으로 입력되었습니다.</span>')
	}	
}

//비밀번호 유효성 체크 signup.jsp
function pw1(){
	var obj = document.fr;
	if(obj.passwd.value== '' || obj.passwd.value != obj.passwdCk.value){
		document.getElementById("alert_pwd").innerHTML=('<span style="color:red;">비밀번호가 일치하지 않습니다.</span>')
		return;		
	}else{
		document.getElementById("alert_pwd").innerHTML=('<span style="color:green;">입력한 비밀번호와 일치합니다.</span>')
	}	
}
//2차비밀번호 유효성 체크 signup.jsp
function pw2(){
	var obj = document.fr;
	if(obj.passwd.value== '' || obj.passwd.value.length < 4 || obj.passwd.value.length > 12){
		document.getElementById("alert_pw").innerHTML=('<span style="color:red;">비밀번호는 4~12자 사이로 입력해주세요.</span>')
		return;		
	}else{
		document.getElementById("alert_pw").innerHTML=('<span style="color:green;">정상적으로 입력되었습니다.</span>')
	}	
}

// 계정삭제 account.jsp
function accountdelete() {
	var answer = confirm("삭제하시겠습니까?");
	if (answer == true) {
		window.location = "accountdelete";
	} else {
		return false;
	}
}