<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>UClick_회원가입</title>
</head>
<body>
	<c:choose>
		<c:when test="${msg eq 'false'}">
			<script type="text/javascript">
				alert("아이디 중복체크를 하세요.");
				window.history.back();
			</script>
		</c:when>
		<c:when test="${msg eq 'equal'}">
			<script type="text/javascript">
				alert("이미 등록된 전화번호입니다. \n 고객센터에 연락하세요");
				window.history.back();
			</script>
		</c:when>
		<c:when test="${msg eq 'true'}">
			<script type="text/javascript">
				alert("회원가입이 완료되었습니다.");
				window.location.href = "login";
			</script>
		</c:when>
	</c:choose>
</body>
</html>