<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- CSS 임포트하기 -->
<link href="<c:url value="/resources/css/mystyle.css" />" rel="stylesheet">
<script src="/resources/js/uclick.js" type="text/javascript"></script>
<title>UClick_User</title>
</head>
<body>
	<c:choose>
		<c:when test="${msg eq 'false'}">
			<script type="text/javascript">
			alert("중복된 전화번호 입니다.");
		</script>
		</c:when>
	</c:choose>
	<div class="wrapper">
		<div class="header">
			<img src="/resources/img/symbol.png" height="100%"><br>
			<ul>
				<li><a href="findAll">HOME</a></li>
				<li><a class="disable"> <%
			 	// 세션 유무 확인
			 	if (session.getAttribute("sessionid") != null) {
			 		%> <!-- 세션이 있을 경우 세션 유저와 아이디로 출력 -->
			 		${sessionname}(${sessionuserid})님 환영합니다.
					<%
				}%>
				</a></li>
				<li style="float: right"><a href="/logout">LOGOUT</a></li>
				<li style="float: right"><a href="/account">ACCOUNT</a></li>

			</ul>
		</div>
		
		<div class="content">
			<div class="sidebar">
				<table style="margin-left: auto; margin-right: auto;">
					<tr>
						<td><a href="/personal"
							style="font-weight: bold; font-size: x-large;">${user.name}님</a>
						</td>
					</tr>
					<tr>
						<td colspan="2" style="text-align: left"><li><b>근무지 :</b> ${user.loc}</li></td>
					</tr>
					<tr>
						<td colspan="2" style="text-align: left"><li><b>부서 :</b> ${user.dept}</li></td>
					</tr>
					<tr>
						<td colspan="2" style="text-align: left"><li><b>직급 :</b> ${user.position}</li></td>
					</tr>
					<tr>
						<td colspan="2" style="text-align: left"><li><b>대표연락처 :</b> ${mainphone}</li></td>
					</tr>
					<tr>
						<c:if test="${empty user.email}"></c:if>
						<c:if test="${!empty user.email}">
							<td style="text-align: left"><li><b>대표이메일 :</b> ${user.email}</li></td>
						</c:if>
					</tr>
				</table>
			</div>
			
			<div class="main">
				<table width="80%">
					<tr>
						<td colspan="3" style="font-weight: bold; font-size: x-large;text-align: left; ">연락처 수정</td>
					</tr>
					<tr>
						<th width="20%">번호</th>
						<th width="60%">연락처</th>
						<th width="20%"></th>
					</tr>
					<c:forEach var="row" items="${user_phones}" varStatus="status">
						<tr>
							<form action="update" method="post" name="up">
								<td>${row.id}</td>
								<td>${row.mobileNo}</td>
								<input type="hidden" name="id" value="${row.id}"> 
								<input type="hidden" name="MobileNO" value="" id="ph${status.index}">
								<td>
									<input type="button" value="수정" onclick="update(${status.index});">
									<input type="button" value="삭제" onclick="location='delete?id=${row.id}'">
								</td>
							</form>
						</tr>
					</c:forEach>
					<c:if test="${user_phones eq '[]'}">
						<tr>
							<td colspan="3">등록된 연락처가 없습니다.</td>
						</tr>
					</c:if>
					<c:set var="count" value="${fn:length(user_phones)}" />
					<c:choose>
						<c:when test="${count>=5}">
							<tr>
								<td colspan="4">연락처 최대 5개 등록 가능합니다.</td>
							</tr>
						</c:when>
						<c:otherwise>
							<tr>
								<form method="post" action="add" name="fr">
									<td></td>
									<td><input type="text" name="mobileNo" onKeyup="inputPhoneNumber(this);" maxlength="13"/></td>
									<td><input type="button" value="추가" onclick="add();"></td>
								</form>
							</tr>
						</c:otherwise>
					</c:choose>
				</table>
				<table width="80%">
					<tr>
						<td colspan="4" style="height: 50px;"></td> 
					</tr>
					<tr>
						<td id="end">
							<input type="button" value="목록" onclick="location='findAll'">
						</td>
					</tr>
				</table>
			</div>
		</div>
		
		<div class="footer">
			<b>Copyright ⓒ  All rights reserved.</b><br>
			MIN GYU KANG<br> 
			경기도 성남시 분당구 서판교로 <br>
			k91.developer@gmail.com<br>
		</div>
	</div>

</body>
</html>