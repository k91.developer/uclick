<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- JSTL을 쓰기 위한 라이브러리 임포트 -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<!-- CSS 임포트하기 -->
<link href="<c:url value="/resources/css/mystyle1.css" />" rel="stylesheet"> 
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>UClick_로그인</title>
<!-- 로그인 입력 폼 유효성 체크 -->
<script type="text/javascript">
	function test() {
		var obj = document.fr;

		if (obj.usesrid.value == '') {
			alert("아이디를 입력하세요");
			obj.usesrid.focus();
			return false;
		} else if (obj.passwd.value == '') {
			alert("비밀번호를 입력해 주세요");
			obj.passwd.focus();
			return false;
		}
		obj.submit();
	}
</script>
</head>
<body>
	<div class="wrapper">
		<div class="header">
			<img src="/resources/img/symbol.png" height="100%">
		</div>
		
		<div class="content">
			<div class="main">
				<center>
					<table cellspacing="0" width="30%">
					<tr>
						<td colspan="2" style="font-weight: bold; font-size: x-large; text-align: center; ">로그인</td>
					</tr>
						<form action="loginOK" name="fr" method="POST">
							<tr>
								<th><label for="id">아이디</label></th>
								<td><input type="text" id="usesrid" name="usesrid"></td>
							</tr>
							<tr>
								<th><label for="password">비밀번호</label></th>
								<td><input type="password" id="passwd" name="passwd"></td>
							</tr>
						</form>
					</table>
					<table cellspacing="0">
						<tr>
							<td>
								<input type="button" value="Login" onclick="test();">
								<input type="button" value="Sign Up" onclick="location='signup'">
							</td>
						</tr>
					</table>
				</center>

			</div>
		</div>
		
		<div class="footer">
			<b>Copyright ⓒ  All rights reserved.</b><br>
			MIN GYU KANG<br> 
			경기도 성남시 분당구 서판교로 <br>
			k91.developer@gmail.com<br>
		</div>
	</div>

</body>
</html>
</body>
</html>