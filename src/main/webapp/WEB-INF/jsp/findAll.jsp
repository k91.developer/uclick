<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- CSS 임포트하기 -->
<link href="<c:url value="/resources/css/mystyle.css" />" rel="stylesheet">
<title>UClick</title>
</head>
<body>

	<div class="wrapper">
		<div class="header">
			<img src="/resources/img/symbol.png" height="100%"><br>
			<ul>
				<li><a class="active" href="findAll">HOME</a></li>
				<li><a class="disable"> <%
			 	// 세션 유무 확인
			 	if (session.getAttribute("sessionid") != null) {
			 		%> <!-- 세션이 있을 경우 세션 유저와 아이디로 출력 -->
			 		${sessionname}(${sessionuserid})님 환영합니다.
					<%
				}%>
				</a></li>
				<li style="float: right"><a href="/logout">LOGOUT</a></li>
				<li style="float: right"><a href="/account">ACCOUNT</a></li>

			</ul>
		</div>

		<div class="content">
			<div class="sidebar">
				<table style="margin-left: auto; margin-right: auto;">
					<tr>
						<td><a href="/personal"
							style="font-weight: bold; font-size: x-large;">${user.name}님</a>
						</td>
					</tr>
					<tr>
						<td colspan="2" style="text-align: left"><li><b>근무지 :</b> ${user.loc}</li></td>
					</tr>
					<tr>
						<td colspan="2" style="text-align: left"><li><b>부서 :</b> ${user.dept}</li></td>
					</tr>
					<tr>
						<td colspan="2" style="text-align: left"><li><b>직급 :</b> ${user.position}</li></td>
					</tr>
					<tr>
						<td colspan="2" style="text-align: left"><li><b>대표연락처 :</b> ${mainphone}</li></td>
					</tr>
					<tr>
						<c:if test="${empty user.email}"></c:if>
						<c:if test="${!empty user.email}">
							<td style="text-align: left"><li><b>대표이메일 :</b> ${user.email}</li></td>
						</c:if>
					</tr>
				</table>
			</div>

			<div class="main">
				<table width="80%">
					<tr>
						<td colspan="4" style="font-weight: bold; font-size: x-large; text-align: left;">사원목록</td>
					</tr>
					<tr>
						<th width=20%>번호</th>
						<th width=30%>이름</th>
						<th width=25%>근무지</th>
						<th width=25%>부서</th>
					</tr>
					<c:forEach var="row" items="${user_list}">
						<tr>
							<td>${row.id}</td>
							<td><a href="/view?page=${page}&user_id=${row.id}">${row.name}</a></td>
							<td>${row.loc}</td>
							<td>${row.dept}</td>
						</tr>
					</c:forEach>
					<c:if test="${user_list eq '[]'}">
						<tr>
							<td colspan="4">등록된 사원이 없습니다.</td>
						</tr>
					</c:if>
					<tr>
						<td colspan="4" style="height: 50px;"></td>
					</tr>
					<center>
						<c:choose>
							<c:when test="${search eq null}">
								<tr>
									<td colspan="5"><a id="page" href="/findAll?page=1">&lt&lt</a>
										<a id="page" href="/findAll?page=${Before}">&lt</a> <c:forEach
											var="i" begin="${Sblock}" end="${Eblock}">
											<c:choose>
												<c:when test="${page==i}">
													<a id="page"
														style="background-color: CadetBlue; color: white;"
														href="/findAll?page=${i}">${i}</a>
												</c:when>
												<c:otherwise>
													<a id="page" href="/findAll?page=${i}">${i}</a>
												</c:otherwise>
											</c:choose>
										</c:forEach> <a id="page" href="/findAll?page=${Next}">&gt</a> <a
										id="page" href="/findAll?page=${pages}">&gt&gt</a></td>
								</tr>
							</c:when>
							<c:when test="${opt eq 'tel'}">
								<tr>
								</tr>
							</c:when>
							<c:otherwise>
								<tr>
									<td colspan="5"><a id="page"
										href="/findAll?page=1&opt=${opt}&search=${search}">&lt&lt</a>
										<a id="page"
										href="/findAll?page=${Before}&opt=${opt}&search=${search}">&lt</a>
										<c:forEach var="i" begin="${Sblock}" end="${Eblock}">
											<c:choose>
												<c:when test="${page==i}">
													<a id="page"
														style="background-color: CadetBlue; color: white;"
														href="/findAll?page=${i}&opt=${opt}&search=${search}">${i}</a>
												</c:when>
												<c:otherwise>
													<a id="page"
														href="/findAll?page=${i}&opt=${opt}&search=${search}">${i}</a>
												</c:otherwise>
											</c:choose>
										</c:forEach> <a id="page"
										href="/findAll?page=${Next}&opt=${opt}&search=${search}">&gt</a>
										<a id="page"
										href="/findAll?page=${pages}&opt=${opt}&search=${search}">&gt&gt</a>
									</td>
								</tr>
							</c:otherwise>
						</c:choose>
						<tr>
							<td colspan="4">
								<form method="get" action="findAll">
									<select name="opt" id="type" onchange="jsChselect(this.value);">
										<option value="name"
											<c:out value="{$map.opt =='name'?'selected':''}"/>>이름</option>
										<option value="loc"
											<c:out value="{$map.opt =='loc'?'selected':''}"/>>근무지</option>
										<option value="dept"
											<c:out value="{$map.opt =='dept'?'selected':''}"/>>부서</option>
										<option value="tel"
											<c:out value="{$map.opt =='tel'?'selected':''}"/>>연락처</option>
									</select> <input type="search" name="search" value="${map.search}">
									<input type=submit value="search">
								</form>
							</td>
						</tr>
				</table>
				</center>
			</div>
		</div>

		<div class="footer">
			<b>Copyright ⓒ All rights reserved.</b><br> MIN GYU KANG<br>
			경기도 성남시 분당구 서판교로 <br> k91.developer@gmail.com<br>
		</div>
	</div>
</body>
</html>