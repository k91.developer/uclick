<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- JSTL을 쓰기 위한 라이브러리 임포트 -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<link href="<c:url value="/resources/css/mystyle1.css" />" rel="stylesheet">
<script src="/resources/js/uclick.js" type="text/javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>UClick_회원가입</title>
</head>
<body>
	<div class="wrapper">
		<div class="header">
			<img src="/resources/img/symbol.png" height="100%">
		</div>
		
		<div class="content">
			<div class="main">
				<center>
					<H1>회원가입</H1>
					<table>
						<form action="signOK" method="get" name="fr">
							<tr>
								<th>아이디*</th>
								<td>
									<input type="text" name="id" onkeyup="idck()">
									<span id="alert_text"><span style="color: #777">아이디를 입력해주세요</span></span>
								</td>
							</tr>
							<tr>
								<th>비밀번호*</th>
								<td>
									<input type="password" name="passwd" onkeyup="pw2()">
									<span id="alert_pw"><span style="color: #777">비밀번호를 입력해주세요</span></span>
								</td>
							</tr>
							<tr>
								<th>비밀번호 확인*</th>
								<td>
									<input type="password" name="passwdCk" onkeyup="pw1()">
									<span id="alert_pwd"><span style="color: #777">비밀번호를 한번 더 입력해주세요</span></span>
								</td>
							</tr>
							<tr>
								<th>이름*</th>
								<td><input type="text" name="username" maxlength="20"></td>
							</tr>
							<tr>
								<th>연락처*</th>
								<td>
									<input type="tel" name="mobileNo1" maxlength="3" style="width: 30%;">- 
									<input type="tel" name="mobileNo2" maxlength="4" style="width: 30%;">-
									<input type="tel" name="mobileNo3" maxlength="4" style="width: 30%;">
								</td>
							</tr>
							<tr>
								<th>근무지*</th>
								<td>
									<select name="loc" style="width: 100%;">
										<option value="">===근무지===</option>
										<option value="강남">강남</option>
										<option value="서초">서초</option>
										<option value="구로">구로</option>
										<option value="판교">판교</option>
										<option value="용인">용인</option>
										<option value="제주">제주</option>
										<option value="US">US</option>
										<option value="Japan">Japan</option>
										<option value="China">Chinan</option>
									</select>
								</td>
							</tr>
							<tr>
								<th>부서*</th>
								<td>
									<select name="dept" style="width: 100%;">
										<option value="">===부서===</option>
										<option value="Accounting">Accounting(회계)</option>
										<option value="Develop">Develop(개발)</option>
										<option value="Personal">Personal(인사)</option>
										<option value="Planning">Planning(기획)</option>
										<option value="R&D">R&D(연구)</option>
										<option value="Sales">Salse(영업)</option>
										<option value="Support">Support(지원)</option>
									</select>
								</td>
							</tr>
							<tr>
								<th>직위*</th>
								<td>
									<select	name="position" style="width: 100%;">
										<option value="">===직위===</option>
										<option value="부장">부장</option>
										<option value="팀장">팀장</option>
										<option value="대리">대리</option>
										<option value="사원">사원</option>
										<option value="인턴">인턴</option>
										<option value="기타">기타</option>
									</select>
								</td>
							</tr>
							<tr>
								<th>이메일</th>
								<td><input type="email" name="email"></td>
							</tr>
					</table>
					<table>
						<tr>
							<td id="end">
								<input type="button" value="Sign Up" onclick="signup();"> 
								<input type="button" value="Cancel" onclick="location='login'">
							</td>
						</tr>
						</form>
					</table>
				</center>
			</div>
		</div>
		
		<div class="footer">
			<b>Copyright ⓒ  All rights reserved.</b><br>
			MIN GYU KANG<br> 
			경기도 성남시 분당구 서판교로 <br>
			k91.developer@gmail.com<br>
		</div>
	</div>
	
</body>
</html>