<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- CSS 임포트하기 -->
<link href="<c:url value="/resources/css/mystyle1.css" />" rel="stylesheet">
<script src="/resources/js/uclick.js" type="text/javascript"></script>
<title>UClick_계정관리</title>
</head>
<body>
	<div class="wrapper">
		<div class="header">
			<img src="/resources/img/symbol.png" height="100%"><br>
			<ul>
				<li><a href="findAll">HOME</a></li>
				<li><a class="disable"> <%
			 	// 세션 유무 확인
			 	if (session.getAttribute("sessionid") != null) {
			 		%> <!-- 세션이 있을 경우 세션 유저와 아이디로 출력 -->
			 		${sessionname}(${sessionuserid})님 환영합니다.
					<%
				}%>
				</a></li>
				<li style="float: right"><a href="/logout">LOGOUT</a></li>
				<li class="active" style="float: right"><a href="/account">ACCOUNT</a></li>

			</ul>
		</div>

		<div class="main">
			<center>
				<table width="50%">
					<tr>
						<td colspan="2" style="font-weight: bold; font-size: x-large; text-align: center; ">${user_account.name}님의 정보</td>
					</tr>
					<tr>
						<th width="15%">사원번호</th>
						<td width="35%">${user_account.id}</td>
					</tr>
					<tr>
						<th>이름</th>
						<td>${user_account.name}</td>
					</tr>
					<tr>
						<th>근무지</th>
						<td>${user_account.loc}</td>
					</tr>
					<tr>
						<th>부서</th>
						<td>${user_account.dept}</td>
					</tr>
					<tr>
						<th>직급</th>
						<td>${user_account.position}</td>
					</tr>
					<tr>
						<th>대표이메일</th>
						<td>${user_account.email}</td>
					</tr>

				</table>
				<table width="50%">
					<tr>
						<td colspan="4" style="height: 50px;"></td> 
					</tr>
					<tr>
						<td id="end">
						<input type="button" value="목록" onclick="location='findAll'">
						</td>
					</tr>
					<tr>
						<td id="end">
							<input type="button" value="변경" onclick="location='accountchange'"> 
							<input type="button" value="탈퇴" onclick="accountdelete();">
						</td>
					</tr>
				</table>
			</center>
		</div>

		<div class="footer">
			<b>Copyright ⓒ  All rights reserved.</b><br>
			MIN GYU KANG<br> 
			경기도 성남시 분당구 서판교로 <br>
			k91.developer@gmail.com<br>
		</div>
	</div>

</body>
</html>