<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>UClcik_계정관리</title>
</head>
<body>
	<c:choose>
		<c:when test="${msg eq 'true'}">
			<script type="text/javascript">
				alert("정상적으로 회원탈퇴가 되었습니다. 그동안 감사합니다.");
				window.location.href = "login";
			</script>
		</c:when>
	</c:choose>
</body>
</html>