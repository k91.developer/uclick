package kr.co.uclick.repository;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.StopWatch;

import kr.co.uclick.configuration.SpringConfiguration;
import kr.co.uclick.entity.Phone;
import kr.co.uclick.entity.User;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = SpringConfiguration.class)
public class UserRepositoryTest {
	
	@Autowired
	private UserRepository userRepository;
	
	@Before
	public void before() {
		System.out.println("=========before==========");
	}
	
	@After
	public void after() {
		System.out.println("=========after==========");
	}
	
	@Ignore
	@Test
	public void test() {
		System.out.println("============[TEST]============");
	}
	
	@Ignore
	@Test
	public void oneToMany() {
		User first = new User("Person1");
		first.addPhone(new Phone("000-0000-0000"));
		first.addPhone(new Phone("000-0000-1111"));
		User second = new User("Person2");
		second.addPhone(new Phone("000-1111-0000"));
		second.addPhone(new Phone("000-1111-1111"));
		second.addPhone(new Phone("000-1111-2222"));
		User third = new User("Person3");
		userRepository.save(first);
		userRepository.save(second);
		userRepository.save(third);
	}
	
	@Ignore
	@Test
	public void findByName() {
		String name = userRepository.findByName("person1").getName();
		Long id = userRepository.findByName("person1").getId();
		System.out.println("[NAME]"+name+"[USER_ID]"+id);
		System.out.println("=============================");
	}
	
	
	@Ignore
	@Test
	public void findAllview() {
		Page<User> page = userRepository.findAll(PageRequest.of(0, 5));
		printPageData("view",page);
	}
	
	
//	@Test
//	public void findAllByName() {
//		Page<User> page = userRepository.findAllByNameContaining("P", PageRequest.of(0, 1));
//		printPageData("NAME",page);
//	}
	
	@Ignore
	@Test
	public void findAllByName() {
		
		StopWatch sw = new StopWatch();
		sw.start();
		
		Page<User> page = userRepository.findAllByNameLike("P", PageRequest.of(0, 1));
		printPageData("NAME",page);
		
		sw.stop();
		System.out.println("1 total time : " + sw.getTotalTimeSeconds());
	}

	private void printPageData(String label, Page<User> page) {
		if(page==null || page.getSize()<=0) {
			return;
		}
		for(int i =0; i<page.getSize(); i++) {
			User be = page.getContent().get(i);
			System.out.println("["+label+"]"+be.getId()+""+be.getName());
		}
	}
	
	@Ignore
	@Test
	public void finduserid() {
		String name = userRepository.findByUserid("Ha").getName();
		System.out.println(name);
	}
		
}
