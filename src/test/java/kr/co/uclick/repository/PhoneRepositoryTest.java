package kr.co.uclick.repository;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.StopWatch;

import kr.co.uclick.configuration.SpringConfiguration;
import kr.co.uclick.entity.Phone;
import kr.co.uclick.entity.User;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = SpringConfiguration.class)
public class PhoneRepositoryTest {

	@Autowired
	private PhoneRepository phoneRepository;

	@Autowired
	private UserRepository userRepository;

	@Ignore
	@Before
	public void before() {
		System.out.println("=====================before=====================");
	}

	@Ignore
	@org.junit.After
	public void After() {
		System.out.println("=====================After=====================");
	}

	@Ignore
	@Test
	public void test() {
		System.out.println("=========[TEST]==========");
	}

	@Ignore
	@Test
	public void oneToMany() {
		User u = new User("person4");
		for (int i = 0; i < 10; i++) {
			Phone p = new Phone();
			if (i % 3 == 0) {
				p.setMobileNo("444-000-1111");
			} else if (i % 3 == 1) {
				p.setMobileNo("444-000-2222");
			} else {
				p.setMobileNo("444-000-3333");
			}
			p.setUser(u);
			userRepository.save(u);
			phoneRepository.save(p);
		}
	}

	@Ignore
	@Test
	public void oneToMany2() {
		User u = userRepository.findByName("person2");
		for (int i = 0; i < 5; i++) {
			Phone p = new Phone();
			if (i % 3 == 0) {
				p.setMobileNo("555-000-1111");
			} else if (i % 3 == 1) {
				p.setMobileNo("555-000-2222");
			} else {
				p.setMobileNo("555-000-3333");
			}
			p.setUser(u);
			phoneRepository.save(p);
		}
	}

	@Ignore
	@Test
	public void findPhoneByUser_id() {
		List<Phone> list = phoneRepository.findPhoneByUser_id((long) 75);
		System.out.println(list);
	}

	private void printPageData(String label, Page<Phone> page) {
		if (page == null || page.getSize() <= 0) {
			return;
		}
		for (int i = 0; i < page.getSize(); i++) {
			Phone be = page.getContent().get(i);
			System.out.println("[" + label + "]" + be.getId() + "" + be.getMobileNo());
		}

	}

	@Ignore
	@Test
	public void findByid() {
		Phone mobile_No = phoneRepository.findById((long) 73).get();
		System.out.println("[MOBILE_NO]" + mobile_No);
	}

	@Ignore
	@Test
	public void list() {
		List<Phone> list = phoneRepository.findAll();
		System.out.println(list);
	}

	@Ignore
	@Test
	public void findAllBymobileNo() {

		StopWatch sw = new StopWatch();
		sw.start();
		List<Phone> p1 = phoneRepository.findAllBymobileNoContaining("-");
		sw.stop();
		System.out.println("1 total time : " + sw.getTotalTimeSeconds());

		sw = new StopWatch();
		sw.start();
		List<Phone> p2 = phoneRepository.findAllBymobileNoContaining("-");
		sw.stop();
		System.out.println("2 total time : " + sw.getTotalTimeSeconds());

		sw = new StopWatch();
		sw.start();
		List<Phone> p3 = phoneRepository.findAllBymobileNoContaining("-");
		sw.stop();
		System.out.println("3 total time : " + sw.getTotalTimeSeconds());

		assertEquals(148, p1.get(0).getId().intValue());
		assertEquals(148, p2.get(0).getId().intValue());
		assertEquals(148, p3.get(0).getId().intValue());
	}

	@Ignore
	@Test
	public void findByMobileNoExist() {
		int phoneNoExists = phoneRepository.count("000-0000-0000");
		System.out.println(phoneNoExists);
	}

}
