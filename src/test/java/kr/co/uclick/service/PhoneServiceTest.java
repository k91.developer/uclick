package kr.co.uclick.service;

import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import kr.co.uclick.configuration.SpringConfiguration;
import kr.co.uclick.entity.Phone;
import kr.co.uclick.entity.User;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = SpringConfiguration.class)
public class PhoneServiceTest {

	@Autowired
	private PhoneService phoneService;

	@Before
	public void before() {
		System.out.println("=====================before=====================");
	}

	@org.junit.After
	public void After() {
		System.out.println("=====================After=====================");
	}
	
	@Ignore
	@Test
	public void test() {
		System.out.println("==========[TEST]==========");
	}

	@Ignore
	@Test
	public void create() {
		phoneService.create(214L, "TEST_NUMBER");
	}

	@Ignore
	@Test
	public void findAll() {
		List<Phone> list = phoneService.findAll();
		
		for (Phone p : list) {
			System.out.println(p.getMobileNo());
		}
	}

	@Ignore
	@Test
	public void findAllByUser_id() {
		List<Phone> list = phoneService.findPhoneByUser_id(97L);
		System.out.println(list);
	}

	@Ignore
	@Test
	public void update() {
		phoneService.update(219L, "PHONE_UPDATE");
	}

	@Ignore
	@Test
	public void delete() {
		phoneService.delete(219L);
	}

	@Ignore
	@Test
	public void PhoneExist() {
		int a = phoneService.findByPhoneNoExist("010-1234-5678");
		System.out.println(a);
	}
	
	@Ignore
	@Test
	public void deleteByIdTest() {
		phoneService.delete(150L);
	}

	@Ignore
	@Test
	public void findAllBymobile() {
		List<Phone> list = phoneService.findPhonesBymobileNO("12");
		
		for (Phone p : list) {
			System.out.println(p.getMobileNo());
		}
	}
	
}
