package kr.co.uclick.service;

import java.util.List;

import org.hibernate.Hibernate;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import kr.co.uclick.configuration.SpringConfiguration;
import kr.co.uclick.entity.User;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = SpringConfiguration.class)
public class UserServiceTest {

	@Autowired
	private UserService userService;

	@Ignore
	@Test
	public void test() {
		System.out.println("======= [TEST] ======");
	}
	
	@Ignore
	@Before
	public void before() {
		System.out.println("======= [Before] ======");
	}
	
	@Ignore
	@org.junit.After
	public void After() {
		System.out.println("======= [After] ======");
	}
	
	@Ignore
	@Test
	public void create() {
		userService.newUser("test", "1234", "test", "010-0000-0000", "test", "test", "test", "test");
	}

	@Ignore
	@Test
	public void findAll() {
		List<User> list = userService.findAll(1);

		for (User u : list) {
			System.out.println(u.getName());
		}

	}

	@Ignore
	@Test
	public void update() {
		userService.update((long) 217, "UPDATE_EMAIL");
	}

	@Ignore
	@Test
	public void delete() {
		userService.delete(217);
	}

	@Ignore
	@Test
	public void findByName() {
		String name = userService.findByName("Person1").getName();
		System.out.println(name);
	}

	@Ignore
	@Test
	public void findById() {
		String name = userService.findById((long) 96).getName();
		System.out.println(name);
	}

	@Ignore
	@Test
	public void findByUserid() {
		String name = userService.findByUserId("Ha").getName();
		System.out.println(name);
	}
	
	@Ignore
	@Test
	public void findAllByName() {
		List<User> list = userService.findAllByName("M", 0);

		for (User u : list) {
			System.out.println(u.getName());
		}
	}
}
